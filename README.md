Awesome hover effects
=====================

## Introduction


This is a standard HTML and CSS based on HTML5 Boilerplate, Nathan Smith's 960 grid and pieces from Bootstrap 3.
Also provided: coding guidelines to make everyting consistent.

Live demo:
http://gojira.be/gitlab/hover/index.html


## Table of contents


1. [What do I get?](#what-do-i-get)
2. [How to use](#how-to-use)
3. [Gulp](#gulp)


## What do I get?

An awesome hover effect library that's easy to implement.


## Gulp

The project includes a Gulp file
